# architecture
import numpy as np
import torch
import torch.nn.functional as F
import math

class PruningANN(torch.nn.Module):
    '''a simple three layer full connected ANN'''
    def __init__(self, nin, nh, nout=7):
        '''
        nin is number of input feature.
        nh is number of neuron in hidden layer.
        nout equals to the number of categories,
        default is 7 in covtype classification problem.
        '''
        super(PruningANN, self).__init__()
        # attributes
        self.nin = nin
        self.nh = nh
        self.nout = nout
        self.hiddenOut = torch.FloatTensor([])  # output of hidden neuron
        self.distinctiveness = torch.FloatTensor([]) # distinctiveness between two hidden neurons
        # two module object
        self.hidden = torch.nn.Linear(nin, nh)   # hidden 1
        self.outlayer = torch.nn.Linear(nh, nout)    # output layer
        
    def forward(self, x):
        # forward calulation
        hiddenIn = self.hidden(x) 
        # hiddenOut = F.relu(hiddenIn)
        hiddenOut = F.sigmoid(hiddenIn)
        outIn = self.outlayer(hiddenOut)
        out = F.sigmoid(outIn) # map to [0,1] for classification
        # store the output of hidden neurons to an array
        # used for later distinctiveness calculation
        self.hiddenOut = hiddenOut.data
        return out
    
    def getHiddenOut(self):
        '''
        get output value of hidden layer
        this function can be called only after `forward`
        size: n_pattern (row) * nh (col)
              where n_pattern is row of x in forward(x)
        '''
        return self.hiddenOut
    
    def getHiddenWeight(self):
        '''
        get weight between hidden layer and output layer (nh by nout)
        only get list(self.outlayer.parameters())[0],
        since list(self.outlayer.parameters())[1] is bias,
        size: nout * nh  (NOT nh * nout)
        '''
        return list(self.outlayer.parameters())[0].data # only return [0], since [1] is bias
        
    def getInputWeight(self):
        '''
        get weight between input layer and hidden layer (nin by nh)
        size: nh * nin (NOT nin * nh)
        '''
        return list(self.hidden.parameters())[0].data
        
    def constHidden(self, threshold):
        '''
        find all the hidden neurons that its max output - min output < threshold
        '''
        res = []
        num = self.hiddenOut.size()[1]
        for i in range(num):
            maxout = max(self.hiddenOut[:,i])
            minout = min(self.hiddenOut[:,i])
            print("the ", i, "-th hidden neuron: max ", maxout, ", min: ", minout)
            if (maxout - minout) < threshold:
                res.append(i)
        return res
    
    def removeOneConstNeuron(self, index):
        '''
        remove the constant neuron, and adjust bias
        1. adjust bias
        2. remove the index-th column in self.hiddenOut
        3. remove the index-th column in hiddenWeight: list(self.outlayer.parameters())[0].data
        4. remove the index-th row in inputWeight: list(self.hidden.parameters())[0].data
        5. remove the index-th row in bias of input: list(self.hidden.parameters())[1].data
        '''
        num = self.hiddenOut.size()[1]
        perserveInd = torch.from_numpy(np.delete(np.arange(num), np.array([index]))).type(torch.LongTensor)
        # 4. adjust bias of output neuron: list(self.outlayer.parameters())[1].data
        #          adding mean(self.hiddenOut[:, index])*(its weight to output neuron)
        meanval = torch.mean(self.hiddenOut[:, index])
        list(self.outlayer.parameters())[1].data += meanval * list(self.outlayer.parameters())[0].data[:, index]
        # 1. remove the index-th column in self.hiddenOut
        self.hiddenOut = self.hiddenOut[:, perserveInd]
        # 2. remove the index-th column in hiddenWeight: list(self.outlayer.parameters())[0].data
        list(self.outlayer.parameters())[0].data = list(self.outlayer.parameters())[0].data[:, perserveInd]
        # 3. remove the index-th row in the inputWeight: list(self.hidden.parameters())[0].data
        list(self.hidden.parameters())[0].data = list(self.hidden.parameters())[0].data[perserveInd,:]
        # 5. remove the index-th row in bias of input: list(self.hidden.parameters())[1].data
        list(self.hidden.parameters())[1].data = list(self.hidden.parameters())[1].data[perserveInd]
        # don't modify the grad in Variable
        
    def removeAllConstNeuron(self, threshold):
        '''remove all const neuron in array'''
        array = self.constHidden(threshold)
        array.sort(reverse=True)
        counter = 0
        for ind in array:
            self.removeOneConstNeuron(ind)
            counter += 1
        print("Has remove ", counter, " constant neurons.")
    
    def getDistinctiveness(self):
        '''find distinctiveness between two hidden neurons'''
        num = self.hiddenOut.size()[1]
        hout = self.hiddenOut - 0.5
        res = []
        for i in range(num):
            vec1 = hout[:,i]
            for j in range(i):
                vec2 = hout[:,j]
                ang = math.acos(torch.matmul(vec1, vec2)/math.sqrt(torch.matmul(vec1,vec1))/math.sqrt(torch.matmul(vec2,vec2)))
                res.append([i,j,ang*180/3.14])
        return res
    
    def similarity(self, angle = 15):
        '''distinctiveness < angle (15 by default) degree'''
        angleArr = np.array(self.getDistinctiveness())
        mask = np.where(angleArr[:,2] < angle)
        return angleArr[mask, 0:2][0]
    
    def complementary(self, angle = 165):
        '''distinctiveness > angle (165 by default) degree'''
        angleArr = np.array(self.getDistinctiveness())
        mask = np.where(angleArr[:,2] > angle)
        return angleArr[mask, 0:2][0]
    
    def removeOneSimilarPair(self, ind1, ind2):
        '''
        remove one neuron in similarity pair. remove ind1, perserve ind2
        1. adjust weights of ind1 to ind2
        2. remove the ind1-th column in self.hiddenOut
        3. remove the ind1-th column in hiddenWeight: list(self.outlayer.parameters())[0].data
        4. remove the ind1-th row in inputWeight: list(self.hidden.parameters())[0].data
        5. remove the index-th row in bias of input: list(self.hidden.parameters())[1].data
        '''
        num = self.hiddenOut.size()[1]
        perserveInd = torch.from_numpy(np.delete(np.arange(num), np.array([ind1]))).type(torch.LongTensor)
        # 1. adjust weight of un-removed neuron: list(self.outlayer.parameters())[0].data[:, ind2]
        #          adding list(self.outlayer.parameters())[0].data[:, ind1] 
        list(self.outlayer.parameters())[0].data[:, ind2] += list(self.outlayer.parameters())[0].data[:, ind1]
        # 2. remove the ind1-th column in self.hiddenOut
        self.hiddenOut = self.hiddenOut[:, perserveInd]
        # 3. remove the ind1-th column in hiddenWeight: list(self.outlayer.parameters())[0].data
        list(self.outlayer.parameters())[0].data = list(self.outlayer.parameters())[0].data[:, perserveInd]
        # 4. remove the ind1-th row in inputWeight: list(self.hidden.parameters())[0].data
        list(self.hidden.parameters())[0].data = list(self.hidden.parameters())[0].data[perserveInd,:]
        # 5. remove the ind1-th row in bias of input: list(self.hidden.parameters())[1].data
        list(self.hidden.parameters())[1].data = list(self.hidden.parameters())[1].data[perserveInd]
    
    def removeAllSimilarPair(self, angle = 15):
        '''remove the similar neuron pairs'''
        simlist = self.similarity(angle)
        counter = 0
        while simlist.shape[0] != 0:
            ind1 = simlist[0, 0]
            ind2 = simlist[0, 1]
            self.removeOneSimilarPair(int(ind1), int(ind2))
            simlist = self.similarity(angle)
            counter += 1
        print("Has remove ", counter, " similarity neurons.")
    
    def removeOneComplementaryPair(self, ind1, ind2):
        '''
        remove one complementary pair, and adjust weight
        1. remove the ind1-th and ind2-th column in self.hiddenOut
        2. remove the ind1-th and ind2-th column in hiddenWeight: list(self.outlayer.parameters())[0].data
        3. remove the ind1-th and ind2-th row in inputWeight: list(self.hidden.parameters())[0].data
        4. remove the index-th row in bias of input: list(self.hidden.parameters())[1].data
        '''        
        num = self.hiddenOut.size()[1]
        perserveInd = torch.from_numpy(np.delete(np.arange(num), np.array([ind1, ind2]))).type(torch.LongTensor)
        # 1. remove the ind1-th and ind2-th column in self.hiddenOut
        self.hiddenOut = self.hiddenOut[:, perserveInd]
        # 2. remove the ind1-th and ind2-th column in hiddenWeight: list(self.outlayer.parameters())[0].data
        list(self.outlayer.parameters())[0].data = list(self.outlayer.parameters())[0].data[:, perserveInd]
        # 3. remove the ind1-th and ind2-th row in the inputWeight: list(self.hidden.parameters())[0].data
        list(self.hidden.parameters())[0].data = list(self.hidden.parameters())[0].data[perserveInd,:]
        # 4. remove the index-th row in bias of input: list(self.hidden.parameters())[1].data
        list(self.hidden.parameters())[1].data = list(self.hidden.parameters())[1].data[perserveInd]
    
    def removeAllComplementaryPair(self, angle=165):
        '''remove all complementary neuron pair in indlist, and adjust weight'''
        complist = self.complementary(angle)
        counter = 0
        while complist.shape[0] != 0:
            ind1 = complist[0, 0]
            ind2 = complist[0, 1]
            self.removeOneComplementaryPair(int(ind1), int(ind2))
            complist = self.complementary(angle)
            counter += 1
        print("Has remove ", 2*counter, " complementary neurons.")
        
    def pruning(self, constThreshold=0.1, similarThreshold=15, complementThreshold=165):
        '''
        1. remove const neuron
        2. remove complementary
        3. remove similarity
        '''
        self.removeAllConstNeuron(constThreshold)
        self.removeAllComplementaryPair(complementThreshold)
        self.removeAllSimilarPair(similarThreshold)

