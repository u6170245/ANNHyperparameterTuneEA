# A framework for evolutionary algorithm in selecting hyperparameters in an ANN
#
# for detail comments and explaination, see framework.ipynb
# 
# Hongbo


import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.utils.data as Data
from torch.autograd import Variable
import torch.nn.functional as F
import math
import gzip
import io
import time
import pruningANN
import os


# decode the binary chromosome to meaning full hyperparameters

def bin2int(pop):
    '''
    convert binary to decimal.
    pop: a two dim numpy array. shape: POPSIZE*nbits.
    '''
    sz = pop.shape[1]
    return pop.dot(2 ** np.arange(sz)[::-1])

def decodeGenome(pop, nBS, npattern, defGenome):
    '''
    NOTICE: nBS and npattern are not used anymore in the latest version. so give them arbitrary value is OK.
    pop: a two dim numpy array. shape: POPSIZE * EncodingBits. Encoding scheme sees :defGenome: below
    nBS: the number of bits to encode BS (batchsize). in the latest version, it is fixed to be 3.
    npattern: number of patterns in training set
    defGenome: if 0,  genome = [EP, LR, BS]
               if 1,  genome = [EP, LR, BG, BG2, WD, BS, NH]
               if 2,  genome = [EP, LR, BG, BG2, WD, BS]
    '''
    # preset parameters
    NHlist = np.array([10 + 10*i for i in range(16)]) # 4 bits
    WDlist = np.array([0, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1]) # 3 bits
    BG2list = np.array([0.999, 0.99, 0.8, 0.5]) # 2 bits
    BGlist = np.array([0.99, 0.9, 0.8, 0.5]) # 2 bits
    LRlist = np.array([ 0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05, 0.1, 0.5 ]) # 3 bits
    EPlist = np.array([10 + i*2 for i in range(16)]) # 4 bits
    nBS = 3
    # bslen = 2**nBS
    # BSlist = np.array([int(npattern/16/bslen)*(i+1) for i in range(bslen)])  # nBS bits
    # BSlist = np.array([10, 30, 100, 300, 1000, 3000, 1e4, 3e4 ])  # 3 bits
    BSlist = np.array([100, 300, 600, 900, 1200, 1800, 2400, 3000])  # 3 bits
    
    # the number of bits to encode a chromosome
    if defGenome == 0:
        nbits = 4 + nBS + 3  # 7 + nBS = 10
    elif defGenome == 1:
        nbits = 4 + nBS + 3 + 2 + 2 + 3 + 4 # 18 + nBS = 21
    elif defGenome == 2:
        nbits = 4 + nBS + 3 + 2 + 2 + 3 # 14 + nBS = 17
    else:
        print("defGenome must be 0, 1 or 2 only")
        return    
    
    # check whether input pop is in the correct shape
    if pop.shape[1] != nbits:
        print("wrong size of chromosome")
        return
    
    # decoding
    
    # EP 5 bits
    popEP = EPlist[bin2int(pop[:, 0:4])]
    # LR 3 bits
    popLR = LRlist[bin2int(pop[:,4:7])]
    
    if defGenome == 0:
        # BS nBS bits
        popBS = BSlist[bin2int(pop[:, 7:(7+nBS)])]
        return np.array([popEP, popLR, popBS]).T
    else:
        # BG 2 bits
        popBG = BGlist[bin2int(pop[:,7:9])]
        # BG2 2 bits
        popBG2 = BG2list[bin2int(pop[:,9:11])]
        # WD 3 bits
        popWD = WDlist[bin2int(pop[:,11:14])]
        # BS nBS bits 
        popBS = BSlist[bin2int(pop[:,14:(14+nBS)])]
        if defGenome == 2:
            return np.array([popEP, popLR, popBG, popBG2, popWD, popBS]).T
        else:
            # NH 3 bits
            popNH = NHlist[bin2int(pop[:, (14+nBS):(18+nBS)])] 
            return np.array([popEP, popLR, popBG, popBG2, popWD, popBS, popNH]).T
                

# read dataset

def readDataForEA():
    '''
    read dataset for hyperparameter selection by evolutionary algorithm
    return: [xytrainEA, xyvalEA]. where the last column is the classification target.
    '''
    # trainEA
    trainEAfilepath = gzip.open("../data/trainEA.data.gz", 'rb')
    # trainEAfilepath = gzip.open("../data/trainANN.data.gz", 'rb')
    trainEAfile = io.BufferedReader(trainEAfilepath)
    ndata = 581012/20
    xytrainEA = np.empty((int(ndata),55))
    start = time.time()
    i = 0
    for line in trainEAfile:
        xytrainEA[i] = np.fromstring(line, dtype=float, sep=",")
        i += 1;
        if i % 100000 == 0:
            print("Has read to line: ", i)
    end = time.time()
    print("use time: ", end-start, " to read data file.")
    trainEAfile.close()
    
    # valEA
    valEAfilepath = gzip.open("../data/validEA.data.gz", 'rb')
    # valEAfilepath = gzip.open("../data/validANN.data.gz", 'rb')
    valEAfile = io.BufferedReader(valEAfilepath)
    ndata = 581012/2/20
    xyvalEA = np.empty((int(ndata),55))
    start = time.time()
    i = 0
    for line in valEAfile:
        xyvalEA[i] = np.fromstring(line, dtype=float, sep=",")
        i += 1;
        if i % 100000 == 0:
            print("Has read to line: ", i)
    end = time.time()
    print("use time: ", end-start, " to read data file.")
    valEAfile.close()
    
    return [xytrainEA, xyvalEA]
    

def getHyperparameter(xytrainEA, nBS, defGenome, chromosome):
    npattern = xytrainEA.shape[0]
    # default value
    BS = 30
    NH = 100
    LR = 0.01
    EP = 2 # 20
    BG = 0.9
    BG2 = 0.999
    WD = 1e-4
    
    # decode binary representation pop to get hyperparameters of ANN
    hyperparm = decodeGenome(np.array([chromosome]), nBS, npattern, defGenome)[0]
    
    if defGenome == 0: # genome = [EP, LR, BS]
        [EP, LR, BS] = hyperparm
    elif defGenome == 1: # genome = [EP, LR, BG, BG2, WD, BS, NH]
        [EP, LR, BG, BG2, WD, BS, NH] = hyperparm
    elif defGenome == 2: #  genome = [EP, LR, BG, BG2, WD, BS]
        [EP, LR, BG, BG2, WD, BS] = hyperparm
    else:
        print("defGenome must be 0, 1 or 2 only")
        return     
    
    EP = int(EP)
    BS = int(BS)
    NH = int(NH)
    
    return [EP, LR, BG, BG2, WD, BS, NH]


# accuracy of valEA dataset 
# it only applies to a single chromosome
# fitness is an array of accuracy, whose size is the size of population

def accuracyANN(xytrainEA, xyvalEA, nBS, defGenome, chromosome):
    '''
    training a two layer fully connected ANN on dataset xytrainEA,
    and return the accuracy of dataset xyvalEA.
    the hyperparameter of the ANN is given by decoding of pop
    chromosome is one dim numpy array with size EcodingBits.
    '''
    # prepare the dataset
    npattern = xytrainEA.shape[0]
    xtrainEA = torch.from_numpy(xytrainEA[:, :-1]).type(torch.FloatTensor)
    ytrainEA = torch.from_numpy(xytrainEA[:, -1] - 1).type(torch.LongTensor)
    
    xvalEA = Variable(torch.from_numpy(xyvalEA[:, :-1]).type(torch.FloatTensor))
    yvalEA = torch.from_numpy(xyvalEA[:, -1] - 1).type(torch.LongTensor)   
    
    # default value
    BS = 30
    NH = 100
    LR = 0.01
    EP = 2 # 20
    BG = 0.9
    BG2 = 0.999
    WD = 1e-4
    
    # decode binary representation pop to get hyperparameters of ANN
    hyperparm = decodeGenome(np.array([chromosome]), nBS, npattern, defGenome)[0]
    
    if defGenome == 0: # genome = [EP, LR, BS]
        [EP, LR, BS] = hyperparm
    elif defGenome == 1: # genome = [EP, LR, BG, BG2, WD, BS, NH]
        [EP, LR, BG, BG2, WD, BS, NH] = hyperparm
    elif defGenome == 2: #  genome = [EP, LR, BG, BG2, WD, BS]
        [EP, LR, BG, BG2, WD, BS] = hyperparm
    else:
        print("defGenome must be 0, 1 or 2 only")
        return     
    
    EP = int(EP)
    BS = int(BS)
    NH = int(NH)
    # print(hyperparm)
    # print([EP, LR, BG, BG2, WD, BS, NH])
    
    # mini batch of training data set
    EAdataset = Data.TensorDataset(xtrainEA, ytrainEA)
    EAloader = Data.DataLoader(
            dataset=EAdataset,
            batch_size=BS,
            shuffle=True,
            num_workers=4, # four threads
    )
    
    
    # network architecture
    ann = pruningANN.PruningANN(54, NH, 7)
    
    # optimizer
    optimizer = torch.optim.Adam(ann.parameters(), lr = LR, betas=(BG, BG2), weight_decay=WD)
    # optimizer = torch.optim.Adam(ann.parameters(), lr = LR)
    
    # loss function
    lossfunc = torch.nn.CrossEntropyLoss()
    
    # training
    start = time.time()
    for epoch in range(EP):
        for step, (x, y) in enumerate(EAloader):
            bx = Variable(x)
            by = Variable(y)
            out = ann(bx)
            loss = lossfunc(out, by)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
        # print('Epoch: ', epoch, ' is finished.')
    end = time.time()
    # print("finish training in ", end-start)
    
    # get accuracy of valEA and return it
    testout = ann(xvalEA)
    pred = torch.max(testout, 1)[1].data.squeeze()
    accuracy = sum(pred == yvalEA) / yvalEA.size(0)
    print('hyperparameter: ', [EP, LR, BG, BG2, WD, BS, NH], ' | train loss: %.4f' % loss.data, ' | validation accuracy: ', accuracy, ' | training time', end - start)    
    
    # return accuracy
    return accuracy
    


# fitness function
# it uses the result of accuracyANN. 
# accuracyANN only applies to a single chromosome.
# fitness is an array of accuracyANN, whose size is the size of population.

def prediction(xytrainEA, xyvalEA, nBS, defGenome, pop):
    '''
    pop is two dim numpy array with size POPSIZE * EcodingBits.
    for each chromosome in pop, train a two layer fully connected ANN and calculate its accuracy of validation set xyvalEA 
    return an array of accuracy.
    '''
    return np.array([accuracyANN(xytrainEA, xyvalEA, nBS, defGenome, chromosome) for chromosome in pop])

def fitness(pred):
    '''
    return an array of which is non-zero positive real number ranging from 1e-3 to 1. 
    '''
    return pred + 1e-3 - np.min(pred) 



# Using bias selection with formula
# $p(i) = \frac{f(i)}{\sum_i^{\textrm{POPSIZE}} f(i)}$
# where $f$ is fitness of each chromosome (accuracyANN of the chromosome)
def selection(pop, fitnessList):
    '''
    pop is a two dim numpy array with size POPSIZE * EncodingBits
    fitnessList is one dime numpy array with size POPSIZE
    '''
    popsz = pop.shape[0]
    idx = np.random.choice(np.arange(popsz), size=popsz, replace=True, p = fitnessList/fitnessList.sum())
    # print(fitnessList/fitnessList.sum())
    # print(idx)
    return pop[idx]



def unifromCrossover(chrom, pop, crossrate):
    '''
    chrom is one dim numpy array with size EncodingBits
    pop    is two dim numpy array with size POPSIZE * EncodingBits. Usually, it is the same as parent
    crossrate is the prob of crossover. For each bit in chromosome, the prob of taking place crossover is 0.5*crossrate
    NOTICE: this function has side effect, chrom will be modified after executing
    '''
    popsz = pop.shape[0]
    nbit = pop.shape[1]
    if np.random.rand() < crossrate:
        # select individual from pop randomly 
        i = np.random.randint(0, popsz, size=1)    
        # choose crossover points(bits), crossover takes place at each bit with prob 0.5*crossrate
        cross_points = np.random.randint(0, 2, size=nbit).astype(np.bool)
        # produce one child
        chrom[cross_points] = pop[i, cross_points]  
    return chrom



def staticMutation(chrom, mutationrate):
    '''
    chrom is one dim numpy array with size EncodingBits
    mutationrate is the rate of mutation for each bit
    NOTICE: this function has side effect, chrom will be modified after executing
    '''
    nbit = chrom.shape[0]
    for point in range(nbit):
        if np.random.rand() < mutationrate:
            chrom[point] = 1 if chrom[point] == 0 else 0
    return chrom


# 1. randomly initialise population
# 2. determine fitness of population
# 3. repeat
#     1. select parents from population
#     2. perform crossover on parents creting population
#     3. perform mutation of population
def trainingEA(nBS, defGenome, crossrate, mutationrate, npop, popsz):
    '''
    nBS: number of bits to encode batch size
    defGenome: if 0,  genome = [EP, LR, BS] # for debug 
               if 1,  genome = [EP, LR, BG, BG2, WD, BS, NH] # determine all the hyperparameter
               if 2,  genome = [EP, LR, BG, BG2, WD, BS] # for comparison with pruning technique
    nBS and defGenome together determines the length of a chromosome.
    crossrate: rate of crossover
    mutationrate: rate of mutation
    npop: number of generations. it determines the termination criterion
    popsz: size of population in each generation.
    return: [chromosomes, accuracy, halloffame, fameaccuracy, famehyper]. where
        chromosomes: a three dim numpy array, (npop, popsz, nbits) # or (N_generation, POPSIZE, nbits)
        accuracy: a two dim numpy array, (npop, popsz) # or (N_generation, POPSIZE)
        halloffame: a two dim numpy array, (npop, nbits). the best fitness chromosome in each generation
        fameaccuracy: a one dim numpy array, (npop). the accuracy of each chromosome in halloffame.
        famehyper: a two dim numpy array. (npop, nhyper = 7). the hyperparameter for each chromosome in hallfofame.
    '''
    start = time.time()
    nhyper = 7
    # initialise population
    if defGenome == 0: # genome = [EP, LR, BS]
        pop =  np.random.randint(0, 2, (popsz, 7+3)) 
    elif defGenome == 1: # genome = [EP, LR, BG, BG2, WD, BS, NH]
        pop =  np.random.randint(0, 2, (popsz, 18+3)) 
    elif defGenome == 2: #  genome = [EP, LR, BG, BG2, WD, BS]
        pop =  np.random.randint(0, 2, (popsz, 14+3)) 
    else:
        print("defGenome must be 0, 1 or 2 only")
        return     
    
    nbit = pop.shape[1]
    
    # define output
    # chromosomes and accuracy has 1-1 corespondence
    # halloffame and fameaccuracy has 1-1 correspondence.
    chromosomes = np.empty((npop, popsz, nbit))  
    accuracy = np.empty((npop, popsz))
    halloffame = np.empty((npop, nbit))
    fameaccuracy = np.empty(npop)
    famehyper = np.empty((npop, nhyper))
    
    
    # read dataset
    [xytrainEA, xyvalEA] = readDataForEA()
    
    # loop till terminatoin
    for i in range(npop):
        
        # for each chromosome in population, training the ANN and get accuracy
        pred = prediction(xytrainEA, xyvalEA, nBS, defGenome, pop)
        
        # determine the fitness of population
        fitnessList = fitness(pred)
        
        # output the result of this generation
        bestidx = np.argmax(fitnessList)
        bestchrom = pop[bestidx, :]
        hyper = getHyperparameter(xytrainEA, nBS, defGenome, bestchrom)
        print("\nBest chromosome: ", bestchrom , "\nwhose hyperparameter: ", hyper , "\nbest accuracy ", pred[bestidx], "\nof Generation ", i, "\nNumber of chromosome: ", np.unique(pop, axis=0).shape[0], "\n")
    
        # save output
        chromosomes[i] = pop
        accuracy[i] = pred
        halloffame[i] = bestchrom
        famehyper[i] = hyper
        fameaccuracy[i] = pred[bestidx]
        
        # selection
        pop = selection(pop, fitnessList) 
        pop2 = pop.copy() # for crossover
        
        # reproduction
        for chrom in pop:
            # crossover
            chrom = unifromCrossover(chrom, pop2, crossrate)
            # mutation
            chrom = staticMutation(chrom, mutationrate)
            
    end = time.time()
    print("EA training in ", end - start)
    
    return [chromosomes, accuracy, halloffame, famehyper, fameaccuracy]
        


# get the best hyperparameters from the EA training
def getBestHyper(chromosomes, accuracy, halloffame, famehyper, fameaccuracy):
    '''
    the input of this function is output of trainingEA
    it returns the chromosome with largest accuracy and its corresponding hyperparameters.
    only `halloffame`, `famehyper` and `fameaccuracy` are used
    '''
    idx = np.argmax(fameaccuracy)
    print("Best chromosome in the EA training is ", halloffame[idx])
    print("Its corresponding ANN hyperparameter is ", famehyper[idx])
    print("The highest accuracy is ", fameaccuracy[idx])
    return [halloffame[idx], famehyper[idx], fameaccuracy[idx]]



# plot the learning curve of EA
def plotEALearningCurve(chromosomes, accuracy, halloffame, famehyper, fameaccuracy):
    '''
    plot the learning curve of EA.
    the x-axis is the generation index
    the y-axis is the accuracy of ANN
    there are two curves, the mean accuracy, max accuracy. 
    min accuracy curve is useless.
    '''
    ngen = fameaccuracy.shape[0]
    maxaccuracy = np.max(accuracy, axis = 1)
    meanaccuracy = np.mean(accuracy, axis = 1)
    minaccuracy = np.min(accuracy, axis = 1)
    plt.xlabel("generation")
    plt.ylabel("accuracy")
    plt.plot(np.arange(ngen), maxaccuracy, label="max accuracy of each generation")
    plt.plot(np.arange(ngen), meanaccuracy, label="mean accuracy of each generation")
    # plt.plot(np.arange(ngen), minaccuracy, label="min accuracy of each generation")
    plt.legend()
    plt.show()
    

def plotHallofFame(chromosomes, accuracy, halloffame, famehyper, fameaccuracy):
    plt.xlabel("generation")
    plt.ylabel("Best Accuracy")
    plt.plot(np.arange(fameaccuracy.shape[0]), fameaccuracy)
    plt.show()
    

#  save the results
def saveEATraining(filename,chromosomes, accuracy, halloffame, famehyper, fameaccuracy):

    os.makedirs("result/" + filename, exist_ok=True)
    # save data
    np.save("result/" + filename + "/chromosomes", chromosomes)
    np.save("result/" + filename + "/accuracy", accuracy)
    np.save("result/" + filename + "/halloffame", halloffame)
    np.save("result/" + filename + "/famehyper", famehyper)
    np.save("result/" + filename + "/fameaccuracy", fameaccuracy)

    # save figure
    ngen = fameaccuracy.shape[0]
    maxaccuracy = np.max(accuracy, axis = 1)
    meanaccuracy = np.mean(accuracy, axis = 1)
    minaccuracy = np.min(accuracy, axis = 1)
    plt.xlabel("generation")
    plt.ylabel("accuracy")
    plt.plot(np.arange(ngen), maxaccuracy, label="max accuracy of each generation")
    plt.plot(np.arange(ngen), meanaccuracy, label="mean accuracy of each generation")
    # plt.plot(np.arange(ngen), minaccuracy, label="min accuracy of each generation")
    plt.legend()
    plt.savefig("result/" + filename + "/learncurveEA.eps", format='eps', dpi = 100, bbox_inches="tight")  
    

####################################3

if __name__ == '__main__': 

    # Step 1. EA Hyperparameter
    # parameter for EA
    POPSIZE = 100 # fitness curve and computational power
    CROSSRATE = 0.8 # fitness curve
    MUTATIONRATE = 0.05 # 1/nbits
    NPOP = 100 # fitness curve converge
    DEFGENOME = 0
    NBS = 3  # use 3 bits to encode the batch size. it must be 3 in the latest version.

    # Step 2. Binary Representation
    poptest = np.random.randint(0, 2, (3, 7+3))
    print("testing bin2int:\n", bin2int(poptest))
    print("testing decodeGenome:\n", decodeGenome(pop=np.random.randint(0, 2, (3, 7+3)), nBS=2, npattern=290506, defGenome=0))
    print("testing decodeGenome:\n", decodeGenome(pop=np.random.randint(0, 2, (3, 18+3)), nBS=2, npattern=290506, defGenome=1))
    print("testing decodeGenome:\n", decodeGenome(pop=np.random.randint(0, 2, (3, 14+3)), nBS=2, npattern=290506, defGenome=2))
    
    # ## Step 3. Evaluation and Fitness
    [xytrainEA, xyvalEA] = readDataForEA()
    print(xytrainEA.shape)
    print(xyvalEA.shape)
    NBS  = 3
    accuracyANN(xytrainEA, xyvalEA, NBS, DEFGENOME, np.random.randint(0, 2, (7+NBS)) )
    pred = prediction(xytrainEA, xyvalEA, NBS, DEFGENOME, np.random.randint(0, 2, (3, 7+NBS)) )
    fitness(pred)
    
    # ## Step 4. Selection
    pop =  np.random.randint(0, 2, (3, 7+NBS)) 
    pred = prediction(xytrainEA, xyvalEA, NBS, DEFGENOME, pop )
    fitnessList = fitness(pred)
    selection(pop, fitnessList)

    # ## Step 5. Variatoin
    chrom = pop.copy()[0]
    print(chrom)
    print(unifromCrossover(chrom, pop, CROSSRATE ))
    print(chrom)
    chrom = pop.copy()[0]
    print(chrom)
    print(staticMutation(chrom, 0.1))
    print(chrom)
    
    # ## Step 6. Termination
    # 
    # Here the most naive termination criterion is used.
    # The algorithm will be terminated after NPOP, say 100, populations.
    
    # ## Step 7. Training EA
    # trainingEA(NBS, DEFGENOME, CROSSRATE, MUTATIONRATE, NPOP, POPSIZE)
    # trainingEA(2, 1, 0.8, 0.05, 10, 20)
    [chromosomes, accuracy, halloffame, famehyper, fameaccuracy] = trainingEA(2, 1, 0.8, 0.05, 10, 10)
    
    # ## Step 8. Analysis and Save Results
    getBestHyper(chromosomes, accuracy, halloffame, famehyper, fameaccuracy)
    plotEALearningCurve(chromosomes, accuracy, halloffame, famehyper, fameaccuracy)
    plotHallofFame(chromosomes, accuracy, halloffame, famehyper, fameaccuracy)
    saveEATraining("0529r1",chromosomes, accuracy, halloffame, famehyper, fameaccuracy)
    
